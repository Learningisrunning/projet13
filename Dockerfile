FROM python:3.10-alpine

WORKDIR .

RUN pip install --upgrade pip
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
RUN pip install six
RUN pip install gunicorn

COPY . .

CMD python manage.py runserver 0.0.0.0:$PORT

