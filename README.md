## Résumé

Site web d'Orange County Lettings

## Développement local

### Prérequis

- Compte GitHub avec accès en lecture à ce repository
- Git CLI
- SQLite3 CLI
- Interpréteur Python, version 3.6 ou supérieure

Dans le reste de la documentation sur le développement local, il est supposé que la commande `python` de votre OS shell exécute l'interpréteur Python ci-dessus (à moins qu'un environnement virtuel ne soit activé).

### macOS / Linux

#### Cloner le repository

- `cd /path/to/put/project/in`
- `git clone https://github.com/OpenClassrooms-Student-Center/Python-OC-Lettings-FR.git`

#### Créer l'environnement virtuel

- `cd /path/to/Python-OC-Lettings-FR`
- `python -m venv venv`
- `apt-get install python3-venv` (Si l'étape précédente comporte des erreurs avec un paquet non trouvé sur Ubuntu)
- Activer l'environnement `source venv/bin/activate`
- Confirmer que la commande `python` exécute l'interpréteur Python dans l'environnement virtuel
`which python`
- Confirmer que la version de l'interpréteur Python est la version 3.6 ou supérieure `python --version`
- Confirmer que la commande `pip` exécute l'exécutable pip dans l'environnement virtuel, `which pip`
- Pour désactiver l'environnement, `deactivate`

#### Exécuter le site

- `cd /path/to/Python-OC-Lettings-FR`
- `source venv/bin/activate`
- `pip install --requirement requirements.txt`
- `python manage.py runserver`
- Aller sur `http://localhost:8000` dans un navigateur.
- Confirmer que le site fonctionne et qu'il est possible de naviguer (vous devriez voir plusieurs profils et locations).

#### Linting

- `cd /path/to/Python-OC-Lettings-FR`
- `source venv/bin/activate`
- `flake8`

#### Tests unitaires

- `cd /path/to/Python-OC-Lettings-FR`
- `source venv/bin/activate`
- `pytest`

#### Base de données

- `cd /path/to/Python-OC-Lettings-FR`
- Ouvrir une session shell `sqlite3`
- Se connecter à la base de données `.open oc-lettings-site.sqlite3`
- Afficher les tables dans la base de données `.tables`
- Afficher les colonnes dans le tableau des profils, `pragma table_info(Python-OC-Lettings-FR_profile);`
- Lancer une requête sur la table des profils, `select user_id, favorite_city from
  Python-OC-Lettings-FR_profile where favorite_city like 'B%';`
- `.quit` pour quitter

#### Panel d'administration

- Aller sur `http://localhost:8000/admin`
- Connectez-vous avec l'utilisateur `admin`, mot de passe `Abc1234!`

### Windows

Utilisation de PowerShell, comme ci-dessus sauf :

- Pour activer l'environnement virtuel, `.\venv\Scripts\Activate.ps1` 
- Remplacer `which <my-command>` par `(Get-Command <my-command>).Path`


### Déploiement
Pour déployer l'application sur HEROKU. 

Il vous faut : 

1. Créer un compte heroku. 
2. Créer une application sur heroku
3. Ensuite aller dans gitlab pour configurer quelque variable (Settings/CICD/VIRIABLES)
4. veuillez configurer les variables suivantes avec vos données :
- $CI_REGISTRY (Veuillez écrire docker.io)
- $CI_REGISTRY_PASSWORD (Correspond à votre password sur gitlab)
- $CI_REGISTRY_USER (Correspond à votre nom d'utilisateur sur gitlab)
- $HEROKU_API_KEY  (Correspond à votre clé privée que vous retrouverez dans votre compte Heroku)
- $HEROKU_APP_NAME (correspond au nom de l'application créé sur Heroku)

### Sentry

Pour pouvoir suivre l'activité de l'application et pouvoir être notifier des bugs de l'application, il vous faut :

1. Vous créer un compte sur Sentry.io
2. Dans le fichiers settings.py de l'application veuillez remplacer le DNS par la valeur correspondant au votre. Veuillez à bien mettre le DNS qui ne contient pas votre clé privée afin de ne pas rendre disponible des données importantes. 

Une fois que tout ça est fait vous pouvez lancer la pipeline et votre application sera ensuite disponible.